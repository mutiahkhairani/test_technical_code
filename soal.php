<!DOCTYPE html>
<html>

<head>
    <title>Tes Technical Code</title>
</head>

<body>
    <form method="POST">
        <tr>
            <td><input type="text" name="angka" placeholder="input angka"></td>
        </tr>
        <br>
        <br>
        <tr>

            <input type="submit" name="generate_segitiga" value="generate segitiga">

        </tr>
        <tr>
            <input type="submit" name="generate_ganjil" value="generate ganjil">
        </tr>
        <tr>
            <input type="submit" name="generate_genap" value="generate genap">
        </tr>
    </form>
    <br> <b>Result</b>
    <br>
    <br>
    <?php

    if (isset($_POST['generate_genap'])) {
        $angka = trim($_POST['angka']);
        for ($i = 0; $i <= $angka; $i++) {
            if ($i % 2 == 0) {
                echo $i . " ";
            }
        }
    }

    if (isset($_POST['generate_ganjil'])) {
        $angka = trim($_POST['angka']);
        for ($i = 0; $i <= $angka; $i++) {
            if ($i % 2 == 1) {
                echo $i . " ";
            }
        }
    }

    if (isset($_POST['generate_segitiga'])) {
        $angka = trim($_POST['angka']);
        for ($i = 1; $i <= $angka; $i++) {
            for ($j = $i; $j >= 1; $j--) {
                echo $i;
            }
            echo "<br>";
        }
    }

    ?>
</body>


</html>